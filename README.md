> ⚠ Please note:  
> This README was retroactively created from the
> [original forum post](http://forum.sa-mp.com/showthread.php?t=158319)
> and was modified slightly to remove stuff like weird typos and expired
> links.

# Teaserbar v1.0

Hi  
I just created a little Teaserbar which allows admins to display texts
on the bottom of the screen.  
It's very simple.

## Commands  
**/teaser** - Displays/Hides the Teaser  
**/teasertext** - Let you change the Text

## Notice
If you use an Admin Script like in Godfather (`PlayerInfo[playerid][pAdmin]`), 
put this in your Gamemode and write down, how the script can identify an Admin.

e.g.
```c
public IsPlayerAdminCall(playerid) {
	return (PlayerInfo[playerid][pAdmin] >= 1337);
}
```

## Planned features:
- Save the teaser state
- More Texts changing automatically
