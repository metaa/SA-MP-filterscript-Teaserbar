/*===================================================================================*\
| Dieses Filterscript wurde von Meta, dem Scripter von "iLife-Community.net" gemacht. |
|                                                                                     |
| Dir ist es erlaubt, mit diesem Code zu tun was du willst, weil das nicht schwer ist |
|                                           :D                                        |
| Wenn du es verwendest, wären Credits ganz nett, aber nicht notwendig                |
\*===================================================================================*/
#include <a_samp>

#define COLOR_LIGHTBLUE 0x33CCFFAA
#define COLOR_LIGHTRED 0xFF6347AA
#define COLOR_GRAD2 0xBFC0C2FF

forward RefreshTeaser();
forward ShowTeaser();
forward HideTeaser();

new bool:TeaserShown = false;
new Text:Teasertext;
new Text:Teaserbox;
new Teasertimer;
new Teaserstate;
new TeaserTa[256] = "_";
new TeaserTn[256] = "_";
new Float:TeaserY = 447.0;

public OnFilterScriptInit()
{
	AntiDeAMX();
	Teaserbox = TextDrawCreate(320.000000, TeaserY, "_");
	TextDrawAlignment(Teaserbox, 2);
	TextDrawBackgroundColor(Teaserbox, 255);
	TextDrawFont(Teaserbox, 1);
	TextDrawLetterSize(Teaserbox, 0.400000, 1.300000);
	TextDrawColor(Teaserbox, -1);
	TextDrawSetOutline(Teaserbox, 1);
	TextDrawSetProportional(Teaserbox, 1);
	TextDrawUseBox(Teaserbox, 1);
	TextDrawBoxColor(Teaserbox, 160);
	TextDrawTextSize(Teaserbox, 0.000000, 636.000000);

		 if(Teaserstate < 11) { Teasertext = TextDrawCreate(320.000000, TeaserY, TeaserTa); }
	else if(Teaserstate > 10) { Teasertext = TextDrawCreate(320.000000, TeaserY, TeaserTn); }
	TextDrawAlignment(Teasertext, 2);
	TextDrawBackgroundColor(Teasertext, 255);
	TextDrawFont(Teasertext, 1);
	TextDrawLetterSize(Teasertext, 0.400000, 1.300000);
	TextDrawColor(Teasertext, -1);
	TextDrawSetOutline(Teasertext, 1);
	TextDrawSetProportional(Teasertext, 1);
	return 1;
}

public OnFilterScriptExit()
{
	TextDrawDestroy(Teasertext);
    TextDrawDestroy(Teaserbox);
	return 1;
}

main()
{
	print("\n                           ___________________");
	print("                           |     Teaserbar    |");
	print("                           |------------------|");
	print("                           |  © 2010 by Meta  |");
	print("                           ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯\n");
}

public OnPlayerConnect(playerid)
{
    if(TeaserShown) { TextDrawShowForPlayer(playerid, Teasertext); TextDrawShowForPlayer(playerid, Teaserbox); }
	return 1;
}

public OnPlayerCommandText(playerid, cmdtext[])
{
    new tmp[256], cmd[256], idx;
    cmd = strtok(cmdtext, idx);
	if(strcmp(cmdtext, "/teaser", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
	        if(Teaser_IsPlayerAdmin(playerid))
	        {
	            if(Teaserstate != 0) { SendClientMessage(playerid, COLOR_LIGHTRED, "Warte kurz!"); return 1; }
	            if(TeaserShown == false)
	            {
	                TeaserShown = true;
					Teasertimer = SetTimer("ShowTeaser", 30, 1);
	                SendClientMessage(playerid, COLOR_LIGHTBLUE, "Der Teaser wird jetzt angezeigt!");
				}
				else
				{
					TeaserShown = false;
					Teasertimer = SetTimer("HideTeaser", 30, 1);
	                SendClientMessage(playerid, COLOR_LIGHTBLUE, "Der Teaser wird jetzt nicht mehr angezeigt!");
				}
			}
		}
	    return 1;
	}
    if(strcmp(cmd, "/teasertext", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
	        if(Teaser_IsPlayerAdmin(playerid))
	        {
	            if(Teaserstate != 0) { SendClientMessage(playerid, COLOR_LIGHTRED, "Warte kurz!"); return 1; }
	            tmp = strtok(cmdtext,idx);
			 	format(TeaserTn, sizeof(TeaserTn), "%s", tmp);
				SendClientMessage(playerid, COLOR_LIGHTBLUE, "Teasertext geändert!");
				if(TeaserShown) { Teasertimer = SetTimer("RefreshTeaser", 30, 1); } else { TeaserTa = TeaserTn; }
			}
		}
	    return 1;
	}
	return 0;
}

public RefreshTeaser()
{
	TextDrawDestroy(Teasertext);
    TextDrawDestroy(Teaserbox);
	if(Teaserstate == 0) { TeaserY = 434.500000; }
	if(Teaserstate == 1) { TeaserY = 435.200000; }
	if(Teaserstate == 2) { TeaserY = 436.200000; }
	if(Teaserstate == 3) { TeaserY = 437.500000; }
	if(Teaserstate == 4) { TeaserY = 439.000000; }
	if(Teaserstate == 5) { TeaserY = 440.500000; }
	if(Teaserstate == 6) { TeaserY = 442.000000; }
	if(Teaserstate == 7) { TeaserY = 443.500000; }
	if(Teaserstate == 8) { TeaserY = 445.500000; }
	if(Teaserstate == 9) { TeaserY = 447.500000; }
	if(Teaserstate == 10) { TeaserY = 449.000000; }
	if(Teaserstate == 11) { TeaserY = 447.500000; }
	if(Teaserstate == 12) { TeaserY = 445.500000; }
	if(Teaserstate == 13) { TeaserY = 443.500000; }
	if(Teaserstate == 14) { TeaserY = 442.000000; }
	if(Teaserstate == 15) { TeaserY = 440.500000; }
	if(Teaserstate == 16) { TeaserY = 439.000000; }
	if(Teaserstate == 17) { TeaserY = 437.500000; }
	if(Teaserstate == 18) { TeaserY = 436.200000; }
	if(Teaserstate == 19) { TeaserY = 435.200000; }
	if(Teaserstate == 20) { TeaserY = 434.500000; }
	if(Teaserstate == 20) { TeaserY = 434.000000; }

	Teaserbox = TextDrawCreate(320.000000, TeaserY, "_");
	TextDrawAlignment(Teaserbox, 2);
	TextDrawBackgroundColor(Teaserbox, 255);
	TextDrawFont(Teaserbox, 1);
	TextDrawLetterSize(Teaserbox, 0.400000, 1.300000);
	TextDrawColor(Teaserbox, -1);
	TextDrawSetOutline(Teaserbox, 1);
	TextDrawSetProportional(Teaserbox, 1);
	TextDrawUseBox(Teaserbox, 1);
	TextDrawBoxColor(Teaserbox, 160);
	TextDrawTextSize(Teaserbox, 0.000000, 636.000000);

		 if(Teaserstate < 11) { Teasertext = TextDrawCreate(320.000000, TeaserY, TeaserTa); }
	else if(Teaserstate > 10) { Teasertext = TextDrawCreate(320.000000, TeaserY, TeaserTn); }
	TextDrawAlignment(Teasertext, 2);
	TextDrawBackgroundColor(Teasertext, 255);
	TextDrawFont(Teasertext, 1);
	TextDrawLetterSize(Teasertext, 0.400000, 1.300000);
	TextDrawColor(Teasertext, -1);
	TextDrawSetOutline(Teasertext, 1);
	TextDrawSetProportional(Teasertext, 1);

	TextDrawShowForAll(Teasertext);
    TextDrawShowForAll(Teaserbox);

    if(Teaserstate == 21) { KillTimer(Teasertimer); Teaserstate = 0; TeaserTa = TeaserTn; return 1; }
	Teaserstate++;
	return 1;
}

public HideTeaser()
{
	TextDrawDestroy(Teasertext);
    TextDrawDestroy(Teaserbox);
	if(Teaserstate == 0) { TeaserY = 434.500000; }
	if(Teaserstate == 1) { TeaserY = 435.200000; }
	if(Teaserstate == 2) { TeaserY = 436.200000; }
	if(Teaserstate == 3) { TeaserY = 437.500000; }
	if(Teaserstate == 4) { TeaserY = 439.000000; }
	if(Teaserstate == 5) { TeaserY = 440.500000; }
	if(Teaserstate == 6) { TeaserY = 442.000000; }
	if(Teaserstate == 7) { TeaserY = 443.500000; }
	if(Teaserstate == 8) { TeaserY = 445.500000; }
	if(Teaserstate == 9) { TeaserY = 447.500000; }
	if(Teaserstate == 10) { TeaserY = 449.000000; }

	Teaserbox = TextDrawCreate(320.000000, TeaserY, "_");
	TextDrawAlignment(Teaserbox, 2);
	TextDrawBackgroundColor(Teaserbox, 255);
	TextDrawFont(Teaserbox, 1);
	TextDrawLetterSize(Teaserbox, 0.400000, 1.300000);
	TextDrawColor(Teaserbox, -1);
	TextDrawSetOutline(Teaserbox, 1);
	TextDrawSetProportional(Teaserbox, 1);
	TextDrawUseBox(Teaserbox, 1);
	TextDrawBoxColor(Teaserbox, 160);
	TextDrawTextSize(Teaserbox, 0.000000, 636.000000);

	Teasertext = TextDrawCreate(320.000000, TeaserY, TeaserTa);
	TextDrawAlignment(Teasertext, 2);
	TextDrawBackgroundColor(Teasertext, 255);
	TextDrawFont(Teasertext, 1);
	TextDrawLetterSize(Teasertext, 0.400000, 1.300000);
	TextDrawColor(Teasertext, -1);
	TextDrawSetOutline(Teasertext, 1);
	TextDrawSetProportional(Teasertext, 1);

	TextDrawShowForAll(Teasertext);
    TextDrawShowForAll(Teaserbox);

    if(Teaserstate == 10) { TextDrawDestroy(Teasertext); TextDrawDestroy(Teaserbox); Teaserstate = 0; TeaserTa = TeaserTn; KillTimer(Teasertimer); return 1; }
	Teaserstate++;
	return 1;
}

public ShowTeaser() // von 449 auf 434
{
	TextDrawDestroy(Teasertext);
    TextDrawDestroy(Teaserbox);
	if(Teaserstate == 0) { TeaserY = 447.500000; }
	if(Teaserstate == 1) { TeaserY = 445.500000; }
	if(Teaserstate == 2) { TeaserY = 443.500000; }
	if(Teaserstate == 3) { TeaserY = 442.000000; }
	if(Teaserstate == 4) { TeaserY = 440.500000; }
	if(Teaserstate == 5) { TeaserY = 439.000000; }
	if(Teaserstate == 6) { TeaserY = 437.500000; }
	if(Teaserstate == 7) { TeaserY = 436.200000; }
	if(Teaserstate == 8) { TeaserY = 435.200000; }
	if(Teaserstate == 9) { TeaserY = 434.500000; }
	if(Teaserstate == 10) { TeaserY = 434.000000; }

	Teaserbox = TextDrawCreate(320.000000, TeaserY, "_");
	TextDrawAlignment(Teaserbox, 2);
	TextDrawBackgroundColor(Teaserbox, 255);
	TextDrawFont(Teaserbox, 1);
	TextDrawLetterSize(Teaserbox, 0.400000, 1.300000);
	TextDrawColor(Teaserbox, -1);
	TextDrawSetOutline(Teaserbox, 1);
	TextDrawSetProportional(Teaserbox, 1);
	TextDrawUseBox(Teaserbox, 1);
	TextDrawBoxColor(Teaserbox, 160);
	TextDrawTextSize(Teaserbox, 0.000000, 636.000000);

	Teasertext = TextDrawCreate(320.000000, TeaserY, TeaserTn);
	TextDrawAlignment(Teasertext, 2);
	TextDrawBackgroundColor(Teasertext, 255);
	TextDrawFont(Teasertext, 1);
	TextDrawLetterSize(Teasertext, 0.400000, 1.300000);
	TextDrawColor(Teasertext, -1);
	TextDrawSetOutline(Teasertext, 1);
	TextDrawSetProportional(Teasertext, 1);

	TextDrawShowForAll(Teasertext);
    TextDrawShowForAll(Teaserbox);

    if(Teaserstate == 10) { KillTimer(Teasertimer); Teaserstate = 0; return 1; }
	Teaserstate++;
	return 1;
}

stock Teaser_IsPlayerAdmin(playerid)
	return (IsPlayerAdmin(playerid) || CallRemoteFunction("IsPlayerAdminCall","i",playerid));

strtok(const string[], &index)
{
    new length = strlen(string);
    while ((index < length) && (string[index] <= ' '))
    {
        index++;
    }

    new offset = index;
    new result[20];
    while ((index < length) && (string[index] > ' ') && ((index - offset) < (sizeof(result) - 1)))
    {
        result[index - offset] = string[index];
        index++;
    }
    result[index - offset] = EOS;
    return result;
}

AntiDeAMX()
{
	new xyz[][] =
    {
		"Unarmed (Fist)",
        "Brass K"
    };
    #pragma unused xyz
}
